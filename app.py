import json
import sys

if len(sys.argv) != 2:
    exit("A length is required. Example: python app.py 16")

# Get length password required
pass_length = int(sys.argv[1])

# Open json file
with open("passwords.json", encoding="utf-8") as f:
    data = json.load(f)

# Checking password length
for i in data["items"]:
    try:
        if len(i["login"]["password"]) < pass_length:
            print(
                i["name"]
                + ": "
                + str(i["login"]["username"])
                + " ("
                + str(len(i["login"]["password"]))
                + ")"
            )
    except Exception as e:
        if "object of type 'NoneType' has no len()" not in str(
            e
        ) and "login" not in str(e):
            print("Fail checking pass in " + i["name"] + ": " + str(e))
        continue

# Closing file
f.close()
