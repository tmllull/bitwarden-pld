# Bitwarden password length checker

Check if any of your passwords has less than specific length.

## Why
Bitwarden already has a "Weak passwords" report tool (https://bitwarden.com/help/reports/), and is a good point to start. The problem is that this tool use a "basic" parameters to definde a good or weak password.

This means that a password with 9 characters can be considered a good password, but currently it's very recomended to have a longer passwords.

If you started to use Bitwarden from scratch, and use always a generated password (with a more than 12-14 chars), there is no reason to use this script. But if you have migrated your old passwords with a lot of manual generated, maybe this script can be useful.

In my case, the Bitwarden report shows me some weak passwords, but a lot of the "good" passwords has less than 10 chars, and for me (and for now) this is not enough.

## How to use

1. Extract your Bitwarden/Vaultwarden vault (without encryption) and save it in the same folder as the script
2. Change this file name to "passwords.json"
3. Run the command `python app.py X`, where X is the minimum password length you want

A list of any entry and the user with less than X chatacters will be displayed in the follow format: `Entry name: username (password length)`. The username is displayed because can be more than 1 entry with the same name (Gmail, for example).

### Docker
If you want to use it with Docker, replace the above step 3 for these:

1. ``docker build --tag bitwarden-pld .``
   
2. ``docker run -it --rm -v /path/to/passwords.json:/app/passwords.json bitwarden-pld app.py X``, where X is the minimum password length you want.

**IMPORTANT**

After use, remember to delete your passwords file.
